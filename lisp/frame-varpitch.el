;;; .doom.d/lisp/frame-varpitch.el -*- lexical-binding: t; -*-

(setq frame-varpitch/current-font doom-font)

(defun frame-varpitch ()
  "Sets `doom-variable-pitch-font' as the frame font,
  and toggles back to `doom-font'. Hacky and assumes
  initial font is fixed pitch."
  (interactive)
  (if (eq frame-varpitch/current-font doom-font)
      (progn
        (set-frame-font doom-variable-pitch-font)
        (setq frame-varpitch/current-font doom-variable-pitch-font))
    (progn
      (set-frame-font doom-font)
      (setq frame-varpitch/current-font doom-font))))
