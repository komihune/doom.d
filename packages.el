;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)
(package! org-sticky-header)
(package! toc-org)
(package! flyspell-popup)
(package! ivy-posframe)
(package! evil-escape :disable t)
(package! spacemacs-theme)
;(package! darklight :recipe (:host github :repo "komihune/darklight"))
(package! darklight :recipe (:host nil :repo "git@github.com:komihune/darklight.el.git"))
(package! nov)
