;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here
(setq doom-font "Iosevka Custom 12"
      doom-variable-pitch-font "Noto Sans"
      doom-big-font "Noto Sans 19"
      doom-theme 'doom-one
      darklight/dark-theme doom-theme
      darklight/light-theme 'doom-solarized-light)

;; swap between fixed and variable pitch fonts
(load! "lisp/frame-varpitch")

(setq-default fill-column 80
              create-lockfiles nil)

(setq ispell-program-name "hunspell")

(delete-selection-mode +1)

;; map `ace-window' to M-o
(map! :g
      "M-o" 'ace-window
      :leader
      (:prefix "t"
        :desc "Swap dark/light theme" "t" 'darklight/swap
        :desc "Mixed pitch mode" "v" 'mixed-pitch-mode)
      (:prefix "o"
        :desc "org tree" "O" 'org-sidebar-tree))

;; restore some normal maps
(map! :i
      "C-SPC" 'set-mark-command
      :g
      "C-x k" 'kill-current-buffer
      "C-x K" 'kill-buffer)

(after! evil
  (setq evil-disable-insert-state-bindings t))

(use-package! org-sticky-header
  :hook (org-mode . org-sticky-header-mode)
  :config (setq org-sticky-header-full-path 'full))

(use-package! toc-org
  :hook (org-mode . toc-org-mode))

(use-package! flyspell-popup
  :after flyspell
  :commands (flyspell-popup-correct)
  :bind (:map flyspell-mode-map
          ("C-;" . flyspell-popup-correct)))

(use-package! ivy-posframe
  :after ivy
  :config
  (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-top-center)))
  (ivy-posframe-mode +1))

(after! org
  (setq org-support-shift-select t ;; allow shift selection in org mode
        org-hide-emphasis-markers t ;; hide // and **, among others
        org-pretty-entities t ;; display e.g. \alpha as α
        org-preview-latex-default-process 'imagemagick ;; set converter for latex previews (formulas) in org mode
        org-use-sub-superscripts t) ;; show sub and superscripts
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 3.0)))

(setq org-export-with-toc nil
      org-export-with-section-numbers nil
      org-export-with-author nil
      org-export-with-date nil
      org-export-with-title nil
      org-latex-compiler "pdflatex"
      org-latex-packages-alist '(("" "libertinus" t)
                                 ("" "fullpage"   nil)))
(add-to-list 'org-latex-packages-alist
             '("AUTO" "babel" t ("pdflatex")))
(add-to-list 'org-latex-packages-alist
             '("AUTO" "polyglossia" t ("xelatex" "lualatex")))
(setq org-latex-classes
   '(("article" "\\documentclass[11pt]{article}"
	  ("\\section{%s}" . "\\section*{%s}")
	  ("\\subsection{%s}" . "\\subsection*{%s}")
	  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	  ("\\paragraph{%s}" . "\\paragraph*{%s}")
	  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	 ("report" "\\documentclass[11pt]{report}"
	  ("\\part{%s}" . "\\part*{%s}")
	  ("\\chapter{%s}" . "\\chapter*{%s}")
	  ("\\section{%s}" . "\\section*{%s}")
	  ("\\subsection{%s}" . "\\subsection*{%s}")
	  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	 ("book" "\\documentclass[11pt]{book}"
	  ("\\part{%s}" . "\\part*{%s}")
	  ("\\chapter{%s}" . "\\chapter*{%s}")
	  ("\\section{%s}" . "\\section*{%s}")
	  ("\\subsection{%s}" . "\\subsection*{%s}")
	  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
	 ("scrbook" "\\documentclass[11pt]{scrbook}"
	  ("\\chapter{%s}" . "\\chapter*{%s}")
	  ("\\section{%s}" . "\\section*{%s}")
	  ("\\subsection{%s}" . "\\subsection*{%s}")
	  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(setq ivy-re-builders-alist
      '((swiper . ivy--regex-plus) ;; use normal matching for swiper...
        (t      . ivy--regex-fuzzy))) ;; ...but fuzzy for everything else

;; bind org-agenda and set agenda dir
(define-key global-map (kbd "C-c a") 'org-agenda)
(setq org-agenda-files "~/org/agenda")

(after! flycheck
  (flycheck-add-mode 'proselint 'latex-mode))

(setq sentence-end-double-space t)

(setq evil-snipe-scope 'visible
      evil-snipe-repeat-scope 'whole-visible
      evil-snipe-spillover-scope 'whole-buffer)

(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Libertinus Serif"
                           :height 1.2))
(add-hook 'nov-mode-hook 'my-nov-font-setup)
